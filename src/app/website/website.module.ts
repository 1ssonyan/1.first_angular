import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { NbActionsModule, NbBadgeModule, NbCardModule, NbIconModule, NbLayoutModule } from '@nebular/theme';
import { ContentComponent } from './content/content.component';
import { WebsiteFooterComponent } from './website-footer/website-footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';


@NgModule({
  declarations: [
    NavbarComponent,
    ContentComponent,
    WebsiteFooterComponent,
    SidebarComponent,

  ],
  imports: [
    CommonModule,
    NbCardModule,
    NbLayoutModule,
    NbActionsModule,
    NbIconModule,
    NbBadgeModule,
  ]
})
export class WebsiteModule { }
